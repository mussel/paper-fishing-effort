# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
# SPDX-License-Identifier: CC0-1.0
# SPDX-FiLeContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
PANDOC=pandoc
SOFFICE=/Applications/LibreOffice.app/Contents/MacOS/soffice

## Markdown extension (e.g. md, markdown, mdown).
MEXT = md

# Markdown source format with extensions

MDFORMAT=--from=markdown+pipe_tables+simple_tables+multiline_tables+fenced_code_blocks+table_captions+yaml_metadata_block+definition_lists+superscript+subscript+tex_math_dollars+inline_notes+smart

BIBCMD=--citeproc #--biblatex --bibliography $(BIB)
#BIBCMD=
# Cross reference options
#CROSSREF = --filter pandoc-crossref -M figPrefix:"Figure" -M eqnPrefix:"Equation" -M tblPrefix:"Table"

TOCCMD=--toc
TOCCMD=

LTX=
CMD=$(PANDOC) $(MDFORMAT) $(BIBCMD) $(TOCCMD)

## All markdown files in the working directory
#SRC = $(wildcard *.$(MEXT))
SRC = Lemmen2024_etal_fishfisheries.$(MEXT)

## Location of Pandoc support files.
PREFIX = $(HOME)/.pandoc
TEMPLATE = wiley-template.tex

PDF=$(SRC:.md=.pdf)
PPTX=$(SRC:.md=.pptx)
HTML=$(SRC:.md=.html)
TEX=$(SRC:.md=.tex)
DOCX=$(SRC:.md=.docx)
ODT=$(SRC:.md=.odt)
EPUB=$(SRC:.md=.epub)
TXT=$(SRC:.md=.txt)
BIB=$(SRC:.md=.bib)

all:  txt tex pdf # odt
pdf:	$(PDF) $(SRC) Makefile
html:	$(HTML) $(SRC) Makefile
docx: $(DOCX) $(SRC) Makefile
odt:  $(ODT) $(SRC) Makefile
epub: $(EPUB) $(SRC) Makefile
tex:  $(TEX) $(SRC) Makefile
txt:  $(TXT) $(SRC) Makefile

%.txt:	%.md %.bib Makefile
	$(CMD) --to=plain -o $@ $<

%.html:	%.md Makefile
	$(CMD) --to=html -o $@ $<

%.epub:	%.md  %.bib Makefile
	$(CMD) --to=epub -s -o $@ $<  # --epub-cover-image=cover-image.gif

%.tex:	%.md  %.bib Makefile style $(TEMPLATE)
	$(CMD) -s  --template=./$(TEMPLATE)  -o $@ $<

%.pdf:	%.md  %.bib Makefile style $(TEMPLATE)
	$(CMD) -s  --pdf-engine=lualatex --template=./$(TEMPLATE)  -o $@ $<

# %.pdf:	%.md Makefile ../templates/oup-template.tex  tex
#	$(CMD) -s  --template=../templates/oup-template.tex  -o $@ $<
#	latexmk -pvc -pdf -e '$$pdflatex=q/xelatex %O -interaction=nonstopmode %S/' -f odd
#	pdflatex odd

# %.docx:	%.md  %.bib Makefile
#	$(CMD)--to=docx --reference-doc=Lemmen2024_etal_fishfisheries.docx -s -o $@ $<
# --reference-doc= --filter pandoc-citeproc --bibliography
# %.docx: %.tex
#	$(CMD)  --from latex  -o $@ $<

#%.docx: %.odt Makefile
#	$(SOFFICE) --headless --convert-to docx --outdir . $<

%.docx: %.md Makefile ices-template.docx
	$(CMD) --to=docx --reference-doc=wiley-template.docx  -o $@ $<

%.odt: %.md Makefile ices-template.odt
	$(CMD) --to=odt --reference-doc=wiley-template.odt  -o $@ $<

clean:
	@rm -f $(PDF) $(SRC:.md=.html) $(SRC:.md=.odt) $(SRC:.md=.docx)
	@rm -f $(PDF:.pdf=.nav) $(PDF:.pdf=.out) $(PDF:.pdf=.tex) $(PDF:.pdf=.snm) $(PDF:.pdf=.toc)
	@rm -f $(PDF:.pdf=.log) $(PDF:.pdf=.synctex.gz) $(PDF:.pdf=.blg)  $(PDF:.pdf=.bbl)
	@rm -f missfont.log $(SRC:.md=.run.xml)
	@rm -f $(EPUB) $(TXT)
	@rm -f allbib.html allbib.pdf

style: WileyNJDv5.cls

bib: odd.bib mdpi-bst
	pandoc allbib.tex -o allbib.html --citeproc --bibliography Lemmen2024_etal_fishfisheries.bib
	pandoc allbib.tex -o allbib.pdf --citeproc --bibliography Lemmen2024_etal_fishfisheries.bib

# This following targets are developer-only ones specific to the developmet
# platform used here and may not work universally
localbib:
	cp $(HOME)/temp/mendeley/MuSSeL-ABM_VMS.bib Lemmen2024_etal_fishfisheries.bib

docker:
	docker run -v $(shell pwd):/home -it ubuntu-pandoc /bin/bash

docker-make:
	docker run -v $(shell pwd):/home -it ubuntu-pandoc make -C /home

Figure_1.pdf: Figure_1.svg
	svg2pdf $< $@

arxiv: tex
	zip Lemmen2024_Oerey_arxiv.zip $(TEX) Figure_1.pdf WileyNJDv5.cls
