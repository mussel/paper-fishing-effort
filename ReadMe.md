<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-FileContributor: Carsten Lemmen
SPDX-License-Identifier: CC0-1.0
-->

This directory contains the sources for generating an article for the
Wiley journal Fish & Fisheries

The manuscript is contained in the `Lemmen204_etal_fishfisheries.md` markdown document, accompanied by a `Lemmen204_etal_fishfisheries.bib` bibliography database.
With the help of the `pandoc` document converter, PDF, Latex, Word and other formats can be built, facilitated by the `Makefile` generator.

A pdf version of the manuscript is generated by the Continuous Deployment.

## Submission

Submission to the journal is managed by Manuscript Central.  The link to access this submission is
https://mc.manuscriptcentral.com/faf?PARAMS=xik_uoHs5tKRpp6uaFq3VatfckqRX25EFMJgYTpKNRot1x2gYufTTRDj6zMjvhMSPeEtaqGyo1efZAGZctJBZq9ofeyMQJr4xFMTLMJttP3yffEYtVBoMdMcqEHzMp5Vim5Cb3QaifHQFEJ6effbzJShk8PVwAjw5RJK8SjR1QoiiXifH2m6BDBU1ZPyMsrj2eandboW98rR4KfuV23WeAU2Sea9A3qTkVKSKmcR1Z65RDWH7HHTzyZPiw52vHMcHYxXs32Rcs5uB3hBQHMcMA1GYxfojnVx

The author guide is available as pdf from https://clarivate.com/webofsciencegroup/wp-content/uploads/sites/2/dlm_uploads/2022/05/S1Manuscripts_AuthorGuide_R1.pdf

* LaTeX files can be zipped and uploaded to the submission system. 
* The commands to include your images in the LaTeX (.tex) main document do not begin with dots or slashes before the image name. For example, the correct command would be
"\includegraphics[xxx]{fig1.eps}" instead of "\includegraphics[xxx]{./fig1.eps}".
* Please provide an abstract of no more than 250 words containing the major keywords.
* Please provide six keywords arranged in alphabetical order. These keywords should not include words in the title.
* The style to use is Fish and Fisheries	American Psychological Association (APA) Reference Style	2 column	lato

